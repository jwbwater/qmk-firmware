#include QMK_KEYBOARD_H

// Each layer gets a name for readability, which is then used in the keymap matrix below.
// The underscores don't mean anything - you can have a layer called STUFF or any other name.
#define _BL 0
#define _FL 1
#define _CL 2

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  /* Keymap _BL: Base Layer (Default Layer)
   */
[_BL] = LAYOUT(
  KC_GESC, KC_1,    KC_2,   KC_3,   KC_4,   KC_5,   KC_6,   KC_7,   KC_8,   KC_9,    KC_0,     KC_MINS,  KC_EQL,   KC_GRV,  KC_BSPC,          KC_F10,
  KC_TAB,  KC_Q,    KC_W,   KC_E,   KC_R,   KC_T,   KC_Y,   KC_U,   KC_I,   KC_O,    KC_P,     KC_LBRC,  KC_RBRC,  KC_BSLS,                   KC_F9,
  KC_LGUI, KC_A,    KC_S,   KC_D,   KC_F,   KC_G,   KC_H,   KC_J,   KC_K,   KC_L,    KC_SCLN,  KC_QUOT,  KC_NUHS,  KC_ENT,
  KC_LSFT, KC_NUBS, KC_Z,   KC_X,   KC_C,   KC_V,   KC_B,   KC_N,   KC_M,   KC_COMM, KC_DOT,   KC_SLSH,  KC_RO,    KC_RSFT,          KC_UP,
  KC_LCTL, MO(_FL), KC_LALT, KC_MHEN,       KC_SPC, KC_SPC,                          KC_HENK,  KC_RALT,  MO(_FL),  KC_RCTL, KC_LEFT, KC_DOWN, KC_RGHT),

  /* Keymap _FL: Function Layer
   */
[_FL] = LAYOUT(
  KC_GRV,  KC_F1,   KC_F2,  KC_F3,  KC_F4,  KC_F5,  KC_F6,  KC_F7,  KC_F8,  KC_F9,   KC_F10,   KC_F11,   KC_F12,   _______, KC_DEL,           KC_PGUP,
  _______, _______, _______,_______,_______,_______,_______,_______,_______,_______, _______,  KC_HOME,  KC_END,   KC_F8,                     KC_PGDN,
  _______, _______, MO(_CL),_______,_______,_______,KC_LEFT,KC_DOWN,KC_UP  ,KC_RGHT, _______,  _______,  _______,  _______,
  MO(_CL), _______, _______,_______,_______,_______,_______,_______,_______,_______, _______,  _______,  _______,  _______,          KC_MS_U,
  KC_BTN1, MO(_FL), KC_BTN2,_______,        _______,_______,                         _______,  _______,  MO(_FL),  MO(_CL), KC_MS_L, KC_MS_D, KC_MS_R),

  /* Keymap _CL: Control layer
   */
[_CL] = LAYOUT(
  _______, _______, _______,_______,_______,_______,_______,_______,_______,_______, KC_PWR ,  KC_SLEP,  KC_WAKE,  _______, _______,         KC_BRIU,
  _______, _______, _______,_______,RESET,  _______,_______,_______,_______,_______, _______,  _______,  _______,  _______,                  KC_BRID,
  _______, _______, MO(_CL),_______,_______,_______,_______,_______,_______,_______, _______,  _______,  _______,  _______,
  _______, _______, _______,_______,_______,_______,_______,_______,_______,_______, _______,  _______,  _______,  _______,          _______,
  _______, MO(_FL), _______,_______,        _______,_______,                         _______,  _______,  MO(_FL),  _______, _______, _______, _______),
};
