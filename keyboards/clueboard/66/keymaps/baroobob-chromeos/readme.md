# My Chrome OS Clueboard 66% Layout

Chrome OS uses F9 and F10 for volume down and volume up so the Ins key in the
upper right hand corner of the Clueboard 66 is mapped to F10 and the Del key
is mapped to F9.

Chrome OS uses KC_LGUI as the search key which is located where caps lock is
normally found. Caps lock is toggled using search + alt. The search key is used
for many keyboard short cuts on Chrome OS.

The h,j,k,l are mapped to arrow keys in the fn layer. For vim users, using
these keys for moving around is second nature.

In the fn layer, the arrow keys move the mouse, left ctrl is the left mouse
button, and left alt is the right mouse button.
