# My First Clueboard 66% Layout

I got rid of all the RGB mappings and made h,j,k,l arrow keys in the fn layer.
Also made the arrow keys move the mouse in the fn layer and mapped left ctrl,
and left alt to the left and right mouse buttons. Most importantly I made the
isolated Ins and Del keys volume up and volume down and while that worked under
Raspian, it does not work on Chrome OS which uses F9 and F10 for volume down
and volume up. 
