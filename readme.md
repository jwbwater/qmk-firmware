# My Custom Clueboard Keymapping

## Clone QMK
```
git clone https://github.com/qmk/qmk_firmware.git
cd qmk_firmware
```

## Follow Instructions at https://docs.qmk.fm/#/newbs
```
util/qmk_install.sh
```

## Flash FC660C / HASU Firmware
And be sure to use sudo with the make command.
```
git clone git@gitlab.com:jwbwater/qmk-firmware.git
cp -r ../qmk-firmware/keyboards/fc660c/keymaps/baroobob/ keyboards/fc660c/keymaps/
sudo make fc660c:baroobob:dfu
put keyboard in programming mode by pressing button on the Hasu controller
```

## Flash Clueboard Firmware
And be sure to use sudo with the make command.
```
git clone git@gitlab.com:jwbwater/qmk-firmware.git
cp -r ../qmk-firmware/keyboards/clueboard/66/keymaps/baroobob-chromeos/ keyboards/clueboard/66/keymaps/
sudo make clueboard/66/rev2:baroobob-chromeos:dfu
put keyboard in programming mode by pressing Fn-S-R
```
